#!/bin/bash

export AKS_CLUSTER_NAME=td-kube-rbac
export RESOURCE_GROUP=td-test-rg
export ACR_NAME=tdacr
export LOCATION="westeurope"
export ACR_URL=$(az acr show --name $ACR_NAME --resource-group $RESOURCE_GROUP --query "loginServer" --output tsv)
export ACR_KEY=$(az acr credential show --name $ACR_NAME --resource-group $RESOURCE_GROUP --query "passwords[0].value" --output tsv)
export CLIENT_ID=$(az aks show --resource-group $RESOURCE_GROUP --name $AKS_CLUSTER_NAME --query "servicePrincipalProfile.clientId" --output tsv)
export ACR_ID=$(az acr show --name $ACR_NAME --resource-group $RESOURCE_GROUP --query "id" --output tsv)
 export AZURE_STORAGE_ACCOUNT=tdgarage
# Get storage key and create share
export AZURE_STORAGE_KEY=$(az storage account keys list -g $RESOURCE_GROUP -n $AZURE_STORAGE_ACCOUNT --query [0].value -o tsv)
# az storage share create -n images \
# --account-name $STORAGE_NAME \
# --account-key $STORAGE_KEY
