package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"math/rand"
	"net/http"
	"time"
)

type IntRange struct {
	min, max int
}

// get next random value within the interval including min and max
func (ir *IntRange) NextRandom() int {
	return rand.Intn(ir.max-ir.min+1) + ir.min
}

func generatek() []int32 {
	var first int
	var last int
	var max int
	// var value32 int32
	max = 20
	kstring := make([]int32, max)
	first = 10240
	last = 10495
	//for i := first; i <= last; i++ {
	//	fmt.Printf("%#c ", i)
	//}
	rand.Seed(time.Now().Unix())
	ir := IntRange{first, last}
	for i := 0; i < max; i++ {
		value32 := int32(ir.NextRandom())
		// fmt.Printf("%#c ", value32)
		kstring[i] = value32
	}
	// fmt.Printf("%#c ", kstring)
	return kstring
}

func indexPageHandler(writer http.ResponseWriter, request *http.Request) {
	rand.Seed(time.Now().Unix())
	// ir := IntRange{first, last}
	writer.Header().Set("Content-Type", "text/html")
	response := fmt.Sprintf("<body> <h1>klingon poetry </h1><h2> %#c </h2> <img src=\"/image\" width=512 height=512 /> </body>", generatek())
	io.WriteString(writer, response)
}

func calculateColor() color.RGBA {
	return color.RGBA{uint8(rand.Intn(255)),
		uint8(rand.Intn(255)),
		uint8(rand.Intn(255)), 255}
}

func imageHandler(writer http.ResponseWriter, request *http.Request) {
	const ImageWidth = 512
	const ImageHeight = 512
	outputimage := image.NewRGBA(image.Rectangle{image.Point{0, 0},
		image.Point{ImageWidth, ImageHeight}})

	for y := 0; y < ImageHeight; y++ {
		for x := 0; x < ImageWidth; x++ {
			c := calculateColor()
			outputimage.Set(x, y, c)
		}
	}
	png.Encode(writer, outputimage)
}

func okHandler(writer http.ResponseWriter, request *http.Request) {
	response := fmt.Sprintf("OK")
	io.WriteString(writer, response)
}

func main() {
	http.HandleFunc("/", indexPageHandler)
	http.HandleFunc("/image", imageHandler)
	http.HandleFunc("/ok", okHandler)
	http.ListenAndServe(":8080", nil)
}
